package main

import (
	"fmt"
	"github.com/BurntSushi/toml"
	log "github.com/Sirupsen/logrus"
	"github.com/alecthomas/kingpin"
	"github.com/chzyer/readline"
	"github.com/pkg/errors"
	//"github.com/go-ldap/ldap"
)

type resieverConfig struct {
	Username   string `toml:"username,omitempty"`
	Password   string
	LdapUrl    string `toml:"ldap_url"`
	LdapBaseDn string `toml:"ldap_base_dn"`
}

var (
	debug      = kingpin.Flag("debug", "Enable debug mode").Short('d').Default("false").Bool()
	configFile = kingpin.Flag("config", "Config file").Short('c').Default("resiever.conf").String()
	pull       = kingpin.Command("pull", "Pull Sieve rules")
	push       = kingpin.Command("push", "Push Sieve rules")
)

func main() {
	cmd := kingpin.Parse()
	// Parse config
	var config resieverConfig
	if _, err := toml.DecodeFile(*configFile, &config); err != nil {
		log.Fatal(err)
	}

	if config.Username == "" {
		if username, err := readline.Line("Username: "); err != nil {
			log.Fatal(errors.Wrap(err, "Couldn't read username"))
		}
		//config.Username = string(strings.TrimRight(username, "\n"))
		config.Username = string(username)
	}

	if password, err := readline.Password("Password: "); err != nil {
		log.Fatal(errors.Wrap(err, "Couldn't read password"))
	}
	config.Password = password

	// Run command
	switch cmd {
	case "pull":
		if err := doPull(); err != nil {
			log.Fatal(err)
		}
	case "push":
		if err := doPush(); err != nil {
			log.Fatal(err)
		}
	}
}

func doPull() (err error) {
	return errors.New("pull not implemented")
	// Auth to LDAP
	// Query for user's rules attribute
	// Write Sieve files
}

func doPush() (err error) {
	return errors.New("push not implemented")
	// Read Sieve files
	// Auth to LDAP
	// Update user's rules attribute
}
